import {Component, OnDestroy, OnInit} from '@angular/core';
// tslint:disable-next-line:import-spacing
import {ActivatedRoute} from '@angular/router';
import {Subscription, combineLatest} from 'rxjs';
import { TableConfigurationResolver } from '../../../registrar-shared/table-configuration.resolvers';
import { map } from 'rxjs/operators';

declare interface TablePathConfiguration {
  name: string;
  alternateName: string;
  show?: boolean;
}

@Component({
  selector: 'app-theses-home',
  templateUrl: './theses-home.component.html',
})
export class ThesesHomeComponent implements OnInit, OnDestroy {
  public paths: Array<TablePathConfiguration> = [];
  public activePaths: Array<TablePathConfiguration> = [];
  public maxActiveTabs = 3;
  // public tabs: Array<any> = [];
  private paramSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute, private _resolver: TableConfigurationResolver) { }

  ngOnInit() {
    this.paramSubscription = combineLatest(
      this._resolver.get('StudentTheses'),
      this._activatedRoute.firstChild.params
    ).pipe(
      map(([tableConfiguration, params ]) => ({tableConfiguration, params}))
    ).subscribe( (results) => {
      if (this.paths.length === 0) {
        this.paths = (results.tableConfiguration as any).paths;
        this.activePaths = this.paths.filter( x => {
          return x.show === true;
        }).slice(0, this.maxActiveTabs);
      }
      const matchPath = new RegExp('^list/' + results.params.list  + '$', 'ig');
      const findItem = this.paths.find( x => {
        return matchPath.test(x.alternateName);
      });
      if (findItem) {
        this.showTab(findItem);
      }
    });
  }

  showTab(item: any) {
    // find if path exists in active paths
    const findIndex = this.activePaths.findIndex( x => {
      return x.alternateName === item.alternateName;
    });
    if (findIndex < 0) {
      if (this.activePaths.length >= this.maxActiveTabs) {
        // remove item at index 0
        this.activePaths.splice(0, 1);
      }
      // add active path
      this.activePaths.push(Object.assign({}, item));
    }
  }


  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
