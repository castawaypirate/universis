import { TableColumnConfiguration } from '@universis/ngx-tables';

export const THESIS_SHARED_COLUMNS: TableColumnConfiguration[] = [
  {
    name: 'thesis/status/id',
    property: 'thesisStatus',
    title: 'Theses.Shared.ThesesId',
    hidden: true,
    optional: true
  },
  {
    name: 'thesis/name',
    property: 'title',
    title: 'Theses.ThesesName',
    hidden: true,
    optional: true
  },
  {
    name: 'thesis/type/name',
    property: 'thesisType',
    title: 'Theses.ThesesType',
    hidden: true,
    optional: true
  },
  {
    name: 'thesis/subject/name',
    property: 'thesisSubject',
    title: 'Theses.ThesisSubject',
    hidden: true,
    optional: true
  },
  {
    name: 'thesis/startYear/alternateName',
    property: 'startYear',
    title: 'Theses.Shared.StartYear',
    hidden: true,
    optional: true
  },
  {
    name: 'thesis/startPeriod/alternateName',
    property: 'startPeriod',
    title: 'Theses.Shared.StartPeriod',
    formatter: 'TranslationFormatter',
    formatString: 'Periods.${value}',
    hidden: true,
    optional: true
  },
  {
    name: 'members',
    property: 'members',
    virtual: true,
    sortable: false,
    title: 'Theses.Shared.Members',
    formatters: [
      {
        formatter: 'TemplateFormatter',
        formatString: '${thesis.members && thesis.members.length ? thesis.members.map(x => x.member && (x.member.familyName + " " + x.member.givenName)).join(", ") : "-"}'
      }
    ],
    hidden: true,
    optional: true
  },
  {
    name: 'thesis/id',
    property: 'thesis',
    hidden: true,
    title: 'Theses.Thesis'
  },
  {
    name: 'thesis/status/alternateName',
    property: 'statusName',
    title: 'Theses.Shared.Status',
    formatters: [
      {
        formatter: 'TranslationFormatter',
        formatString: 'ThesisStatuses.${value}'
      }
    ],
    hidden: true,
    optional: true
  },
  {
    name: 'thesis/dateModified',
    property: 'dateModified',
    title: 'Theses.Shared.DateModified',
    formatter: 'DateTimeFormatter',
    formatString: 'short',
    hidden: true,
    optional: true
  }
]
