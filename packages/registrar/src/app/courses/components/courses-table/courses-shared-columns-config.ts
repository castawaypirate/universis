import { TableColumnConfiguration } from '@universis/ngx-tables';

export const COURSE_SHARED_COLUMNS: TableColumnConfiguration[] = [
  {
    name: 'id',
    property: 'courseId',
    title: 'Courses.SharedColumns.Id',
    hidden: true,
    optional: true
  },
  {
    name: 'displayCode',
    property: 'courseDisplayCode',
    title: 'Courses.SharedColumns.DisplayCode',
    hidden: true,
    optional: true
  },
  {
    name: 'name',
    property: 'courseName',
    title: 'Courses.SharedColumns.Name',
    hidden: true,
    optional: true
  },
  {
    name: 'isEnabled',
    property: 'courseEnabled',
    title: 'Courses.SharedColumns.Enabled',
    formatter: 'TrueFalseFormatter',
    hidden: true,
    optional: true
  },
  {
    name: 'ects',
    property: 'courseEcts',
    title: 'Courses.SharedColumns.Ects',
    hidden: true,
    optional: true
  },
  {
    name: 'units',
    property: 'courseUnits',
    title: 'Courses.SharedColumns.Units',
    hidden: true,
    optional: true
  },
  {
    name: 'department/abbreviation',
    property: 'courseDepartmentAbbreviation',
    title: 'Courses.SharedColumns.Department',
    hidden: true,
    optional: true
  },
  {
    name: 'courseArea/name',
    property: 'courseAreaName',
    title: 'Courses.SharedColumns.CourseArea',
    hidden: true,
    optional: true
  },
  {
    name: 'courseSector/name',
    property: 'courseSectorName',
    title: 'Courses.SharedColumns.CourseSector',
    hidden: true,
    optional: true
  },
  {
    name: 'courseCategory/name',
    property: 'courseCategoryName',
    title: 'Courses.courseCategory',
    hidden: true,
    optional: true
  },
  {
    name: 'courseStructureType/name',
    property: 'courseStructureTypeName',
    title: 'Courses.courseStructureType',
    formatter: 'TranslationFormatter',
    formatString: 'Courses.CourseType.${value}',
    hidden: true,
    optional: true
  },
  {
    name: 'isShared',
    property: 'courseIsShared',
    title: 'Courses.SharedColumns.Shared',
    formatter: 'TrueFalseFormatter',
    hidden: true,
    optional: true
  },
  {
    name: 'gradeScale/name',
    property: 'courseGradeScaleName',
    title: 'Courses.SharedColumns.GradeScale',
    hidden: true,
    optional: true
  }
]
