import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { LoadingService, ToastService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { AdvancedFormComponent } from '@universis/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-change-course-grade-scale',
  templateUrl: './change-course-grade-scale.component.html',
  styleUrls: ['./change-course-grade-scale.component.scss'],
})
export class ChangeCourseGradeScaleComponent
  extends RouterModalOkCancel
  implements OnInit, OnDestroy
{
  public course: any;
  private paramsSubscription: Subscription;
  private formChangeSubscription: Subscription;
  public lastError: any;
  public gradeScaleChange: {
    allow: boolean;
    message?: string;
  };
  @ViewChild('formComponent') formComponent: AdvancedFormComponent;

  constructor(
    protected _router: Router,
    protected _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _loadingService: LoadingService,
    private _translateService: TranslateService,
    private _toastService: ToastService
  ) {
    // call super constructor
    super(_router, _activatedRoute);
    // override-set class
    this.modalClass = 'modal-xl';
    // set default value before rendering
    this.gradeScaleChange = {
      allow: false,
    };
  }

  ngOnInit() {
    this.paramsSubscription = this._activatedRoute.data.subscribe(
      async (resolvedData) => {
        // get resolved course data
        this.course = resolvedData && resolvedData.data;
        // run basic precheck
        await this.preCheck();
      }
    );
    this.formChangeSubscription = this.formComponent.form.change.subscribe(
      (event) => {
        if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
          // enable or disable button based on form status
          this.okButtonDisabled = !(
            this.gradeScaleChange.allow && event.isValid
          );
        }
        // validate distinctly because of multiple formio change emits (first load)
        const newGradeScale = event.data && event.data.newGradeScale;
        if (newGradeScale == null || Object.keys(newGradeScale).length === 0) {
          this.okButtonDisabled = true;
        }
      }
    );
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }

  async ok() {
    // clear error, if any
    this.lastError = null;
    let hasBeenUpdated = false;
    try {
      // show loading
      this._loadingService.showLoading();
      // get component data
      const formData =
        this.formComponent &&
        this.formComponent.form &&
        this.formComponent.form.formio &&
        this.formComponent.form.formio.data;
      // if target scale is not set
      if (
        formData == null ||
        formData.newGradeScale == null ||
        Object.keys(formData.newGradeScale).length === 0
      ) {
        // close
        return this.close();
      }
      // prepare course object for update
      const updateCourseGradeScale = {
        id: this.course.id,
        courseStructureType: this.course.courseStructureType,
        gradeScale: formData.newGradeScale,
        $state: 2,
      };
      // if the course is simple
      // (can be safely checked from the course attribute, it does not change)
      if (this.course.courseStructureType === 1) {
        // save
        await this._context.model('Courses').save(updateCourseGradeScale);
        hasBeenUpdated = true;
        // and close with reload fragment
        return this.close({fragment: 'reload'});
      } else if (this.course.courseStructureType === 4) {
        // if the course is complex
        // and the changeCoursePartsScale option is checked
        if (formData.changeCoursePartsScale) {
          // get all course parts (apply top -1 for some edge cases)
          const courseParts = await this._context
            .model('Courses')
            .where('parentCourse')
            .equal(this.course.id)
            .select('id', 'courseStructureType')
            .take(-1)
            .getItems();
          // validate course parts
          if (!(Array.isArray(courseParts) && courseParts.length)) {
            throw new Error(
              'Course parts cannot be found or are inaccessible.'
            );
          }
          // set new grade scale to all parts
          courseParts.forEach((coursePart) => {
            coursePart.gradeScale = formData.newGradeScale;
            coursePart.$state = 2;
          });
          // push self to array
          courseParts.push(updateCourseGradeScale);
          // save
          // important note: update all courses in the same transaction
          await this._context.model('Courses').save(courseParts);
          hasBeenUpdated = true;
          // and close with reload fragment
          return this.close({fragment: 'reload'});
        }
        // if changeCoursePartsScale is not set
        // update just self
        await this._context.model('Courses').save(updateCourseGradeScale);
        hasBeenUpdated = true;
        // and close with reload fragment
        return this.close({fragment: 'reload'});
      } else if (this.course.courseStructureType === 8) {
        // if the course is a course part
        // and the changeParentAndSiblingsScale option is checked
        if (formData.changeParentAndSiblingsScale) {
          // get parent course and all other parts
          const parentCourse = await this._context
            .model('Courses')
            .where('id')
            .equal(this.course.parentCourse)
            .select('id', 'courseStructureType', 'courseParts')
            .expand('courseParts($select=id, courseStructureType)')
            .getItem();
          if (parentCourse == null) {
            throw new Error(
              'Parent course cannot be found or is inaccessible.'
            );
          }
          const siblingsAndParentCourses = parentCourse.courseParts;
          if (
            !(
              Array.isArray(siblingsAndParentCourses) &&
              siblingsAndParentCourses.length
            )
          ) {
            throw new Error(
              'The course parts cannot be found or are inaccessible.'
            );
          }
          // set new scale for all course parts
          siblingsAndParentCourses.forEach((course) => {
            course.gradeScale = formData.newGradeScale;
            course.$state = 2;
          });
          // push also parent course object
          siblingsAndParentCourses.push({
            id: parentCourse.id,
            courseStructureType: parentCourse.courseStructureType,
            gradeScale: formData.newGradeScale,
            $state: 2,
          });
          // save
          // important note: update all courses in the same transaction
          await this._context.model('Courses').save(siblingsAndParentCourses);
          hasBeenUpdated = true;
          // and close with reload fragment
          return this.close({fragment: 'reload'});
        }
        // if changeCoursePartsScale is not set
        // update just self
        await this._context.model('Courses').save(updateCourseGradeScale);
        hasBeenUpdated = true;
        // and close with reload fragment
        return this.close({fragment: 'reload'});
      } else {
        throw new Error('Unsupported or missing course structure type.');
      }
    } catch (err) {
      // log and set last error
      console.error(err);
      this.lastError = err;
    } finally {
      // hide loading
      this._loadingService.hideLoading();
      // if the course has been updated
      if (hasBeenUpdated) {
        // show a success toast
        const toastTitle = this._translateService.instant(
          'Courses.ChangeGradeScaleAction.Title'
        );
        const toastMessage = this._translateService.instant(
          'Courses.ChangeGradeScaleAction.ToastMessage'
        );
        this._toastService.show(toastTitle, toastMessage);
      }
    }
  }

  async preCheck() {
    try {
      this._loadingService.showLoading();
      // run a basic validation for this course
      // let the api handle the more complex validations (e.g complex course, course parts)
      // try to find if there is some grade from a submission for this course
      const scaleHasBeenUsedInTrace = await this._context
        .model('StudentGradeActionTraces')
        .where('course')
        .equal(this.course.id)
        .and('examGrade')
        .notEqual(null)
        .select('id')
        .getItem();
      if (!!scaleHasBeenUsedInTrace) {
        // set message
        this.gradeScaleChange.message = this._translateService.instant(
          'Courses.ChangeGradeScaleAction.ScaleHasBeenUsedForGrading'
        );
        return;
      }
      // as a second step, try to find if some student course exists for this course
      const scaleHasBeenUsedInStudentCourse = await this._context
        .model('StudentCourses')
        .where('course')
        .equal(this.course.id)
        .and('grade')
        .notEqual(null)
        .select('id')
        .getItem();
      if (!!scaleHasBeenUsedInStudentCourse) {
        // set message
        this.gradeScaleChange.message = this._translateService.instant(
          'Courses.ChangeGradeScaleAction.ScaleHasBeenUsedForGrading'
        );
        return;
      }
      // allow change after successful validations
      this.gradeScaleChange.allow = true;
    } catch (err) {
      // log error
      console.error(err);
      // set last error
      this.lastError = err;
    } finally {
      this._loadingService.hideLoading();
    }
  }

  async cancel() {
    return this.close();
  }
}
