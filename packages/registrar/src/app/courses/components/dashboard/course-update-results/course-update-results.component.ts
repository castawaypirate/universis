import {Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Observable, Subscription, combineLatest} from 'rxjs';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {ErrorService, LoadingService, ModalService} from '@universis/common';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-course-update-results',
  templateUrl: './course-update-results.component.html',
  styleUrls: []
})
export class CourseUpdateResultsComponent implements OnInit, OnDestroy {

  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  public courseId: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  private selectedItems = [];
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _errorService: ErrorService
  ) {
  }

  async ngOnInit() {
    this.subscription = combineLatest(
        this._activatedRoute.params,
        this._activatedRoute.data
      ).pipe(
        map(([params, data]) => ({params, data}))
      ).subscribe(async (results) => {
      this.courseId = results.params.id;
      this._activatedTable.activeTable = this.students;
      this.students.query = this._context.model('StudentCourseUpdateResults')
        .asQueryable()
        .where('action/course').equal(results.params.id)
        .prepare();
      this.students.config = AdvancedTableConfiguration.cast(results.data.tableConfiguration);
      this.students.fetch();
      // declare model for advance search filter criteria
      this.students.config.model = 'StudentCourseUpdateResults';
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });
      if (results.data.searchConfiguration) {
        this.search.form =  Object.assign(results.data.searchConfiguration, { course: this.courseId });
        this.search.ngOnInit();
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
