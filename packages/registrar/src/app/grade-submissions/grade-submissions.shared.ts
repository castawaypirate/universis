import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import { StudentsSharedModule } from '../students/students.shared';
import { CoursesSharedModule } from '../courses/courses.shared';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    StudentsSharedModule,
    CoursesSharedModule
  ],
  declarations: [
  ],
  exports: [
  ],
  providers: [
  ]
})
export class GradeSubmissionsSharedModule implements OnInit {
  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch((err) => {
      console.error('An error occurred while loading exams shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach((language) => {
      import(`./i18n/grade-submissions.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }
}
