import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardExamParticipateSummaryComponent } from './dashboard-exam-participate-summary.component';

describe('DashboardExamParticipateSummaryComponent', () => {
  let component: DashboardExamParticipateSummaryComponent;
  let fixture: ComponentFixture<DashboardExamParticipateSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardExamParticipateSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardExamParticipateSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
