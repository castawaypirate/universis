import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {TemplatePipe, UserActivityService} from '@universis/common';
import * as GRADUATIONS_LIST_CONFIG from '../graduations-table/graduations-table.config.list.json';
import {TranslateService} from '@ngx-translate/core';
import { cloneDeep } from 'lodash';
import { TableConfiguration } from '@universis/ngx-tables';

@Component({
  selector: 'app-graduations-root',
  templateUrl: './graduations-root.component.html'
})
export class GraduationsRootComponent implements OnInit {

  public model: any;
  public tabs: any[];
  public graduationEventId: any;
  public actions: any[];
  public config: any;
  public allowedActions: any[];
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService,
              private _context: AngularDataContext,
              private _template: TemplatePipe) { }

  async ngOnInit() {
    this.graduationEventId = this._activatedRoute.snapshot.params.id;

    this.model = await this._context.model('GraduationEvents')
      .where('id').equal(this.graduationEventId)
      // .expand('...')
      .getItem();

    this.tabs = this._activatedRoute.routeConfig.children.filter(route => typeof route.redirectTo === 'undefined');
    // @ts-ignore
    this.config = cloneDeep(GRADUATIONS_LIST_CONFIG as TableConfiguration);

    if (this.config.columns && this.model) {
      // get actions from config file
      this.actions = this.config.columns.filter(x => {
        return x.actions;
      })
        // map actions
        .map(x => x.actions)
        // get list items
        .reduce((a, b) => b, 0);

      this.allowedActions = this.actions.filter(x => {
        if (x.role) {
          if (x.role === 'action') {
            return x;
          }
        }
      });

      this.edit = this.actions.find(x => {
        if (x.role === 'edit') {
          x.href = this._template.transform(x.href, this.model);
          return x;
        }
      });

      this.actions = this.allowedActions;
      this.actions.forEach(action => {
        action.href = this._template.transform(action.href, this.model);
      });
    }
    // save user activity for graduation
    return this._userActivityService.setItem({
      category: this._translateService.instant('Graduations.TitleSingular'),
      description: this._translateService.instant(this.model.name),
      url: window.location.hash.substring(1),
      dateCreated: new Date()
    });
  }

}
