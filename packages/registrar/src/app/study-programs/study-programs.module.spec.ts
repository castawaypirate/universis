import { StudyProgramsModule } from './study-programs.module';

describe('StudyProgramsModule', () => {
  let studyProgramsModule: StudyProgramsModule;

  beforeEach(() => {
    studyProgramsModule = new StudyProgramsModule();
  });

  it('should create an instance', () => {
    expect(studyProgramsModule).toBeTruthy();
  });
});
