import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsPreviewComponent } from './study-programs-preview.component';

describe('StudyProgramsPreviewComponent', () => {
  let component: StudyProgramsPreviewComponent;
  let fixture: ComponentFixture<StudyProgramsPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
