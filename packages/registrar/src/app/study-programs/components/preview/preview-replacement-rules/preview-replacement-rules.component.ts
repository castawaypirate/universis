import {Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as REPLACEMENT_LIST_CONFIG from './preview-replacement-rules.config.list.json';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedTableService} from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';


@Component({
  selector: 'app-preview-replacement-rules',
  templateUrl: './preview-replacement-rules.component.html',
})

export class PreviewReplacementRulesComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>REPLACEMENT_LIST_CONFIG;
  @ViewChild('rules') replacementRules: AdvancedTableComponent;
  @Input() searchConfiguration: any;
  @ViewChild('search') search: AdvancedSearchFormComponent;

  studyProgram: any = this._activatedRoute.snapshot.params.id;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
  ) {
  }

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this._activatedTable.activeTable = this.replacementRules;
      this.studyProgram = params.id;
      this.replacementRules.query = this._context.model('StudyProgramReplacementRules')
        .where('studyProgram').equal(params.id)
        .prepare();
      this.replacementRules.config = AdvancedTableConfiguration.cast(REPLACEMENT_LIST_CONFIG);
      this.replacementRules.fetch();
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.replacementRules.fetch(true);
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  remove() {
    if (this.replacementRules && this.replacementRules.selected && this.replacementRules.selected.length) {
      // get items to remove
      const items = this.replacementRules.selected.map(item => {
        return {
          id: item.id
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('StudyPrograms.RemoveReplacementRuleTitle'),
        this._translateService.instant('StudyPrograms.RemoveReplacementRuleMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          this._context.model('StudyProgramReplacementRules').remove(items).then(() => {
            this._toastService.show(
              this._translateService.instant('StudyPrograms.RemoveReplacementRule.title'),
              this._translateService.instant((items.length === 1 ?
                'StudyPrograms.RemoveReplacementRule.one' : 'StudyPrograms.RemoveReplacementRule.many')
                , {value: items.length})
            );
            this.replacementRules.fetch(true);
          }).catch(err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }
}
