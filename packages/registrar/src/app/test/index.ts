
import {SettingsConfiguration} from '@universis/common';

export class TestingConfigurationService {
    public config: any;
    private currentLang: string;
    constructor() {
        this.config = {
            settings: {
                i18n: {
                    locales: [ 'en', 'el' ],
                    defaultLocale: 'el'
                }
            }
        };
        this.currentLang = this.config.settings.i18n.defaultLocale;
    }

    get settings(): SettingsConfiguration {
        return this.config.settings;
    }

    getCurrentLang(): string {
        return this.currentLang;
    }

    setCurrentLang(lang: string) {
        this.currentLang = lang;
    }
}
