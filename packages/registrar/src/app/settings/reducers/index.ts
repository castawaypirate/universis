import { createSelector } from "@ngrx/store";
import { SettingsActions, SettingsActionTypes } from "../actions";

export interface SettingsState {
    filter?: string;
}

const initialState = {
    filter: ''
}

export function filterReducer(state = initialState, action: SettingsActions): SettingsState {
    switch (action.type) {
      case SettingsActionTypes.LoadFilter:
        return state;
      case SettingsActionTypes.AppendFilter:
        state = {
          ...state,
          filter: action.filter
        };
        return state;
      default:
        return state;
    }
  }

  export const selectFilter = (state: any) => state.settings;
 
  export const selectFilterText = createSelector(
    selectFilter,
    (state: SettingsState) => state.filter
  );
  
  