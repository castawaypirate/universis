import {Injectable} from '@angular/core';
import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class DepartmentsTableConfigurationResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./departments-table.config.${route.params.list}.json`)
      .catch( err => {
        return  import(`./departments-table.config.json`);
      });
  }
}

export class DepartmentsTableSearchResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./departments-table.search.${route.params.list}.json`)
      .catch( err => {
        return  import(`./departments-table.search.json`);
      });
  }
}

export class DepartmentsTDefaultTableConfigurationResolver implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return import(`./departments-table.config.json`);
  }
}
