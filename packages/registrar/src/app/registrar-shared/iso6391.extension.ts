import * as ISO6391 from 'iso-639-1/build';

const EXTRA_LANGUAGE_LIST = {
    grc: {
        name: 'Ancient Greek',
        nativeName: 'Αρχαία Ελληνικά',
      }
};

const superGetNativeName = ISO6391.getNativeName;
const superGetName = ISO6391.getName;
const superGetCode = ISO6391.getCode;
const superGetAllNames = ISO6391.getAllNames;
const superGetAllNativeNames = ISO6391.getAllNativeNames;
const superGetAllCodes = ISO6391.getAllCodes;
const superValidate = ISO6391.validate;

function getNativeName(code: string): string {
    if (Object.prototype.hasOwnProperty.call(EXTRA_LANGUAGE_LIST, code)) {
        return EXTRA_LANGUAGE_LIST[code].nativeName;
    }
    return superGetNativeName(code);
}

function getName(code: string): string {
    if (Object.prototype.hasOwnProperty.call(EXTRA_LANGUAGE_LIST, code)) {
        return EXTRA_LANGUAGE_LIST[code].name;
    }
    return superGetName(code);
}

function getCode(name: string): string {
    const code = Object.keys(EXTRA_LANGUAGE_LIST).find((language) => {
        return EXTRA_LANGUAGE_LIST[language].name === name;
    });
    if (code) {
        return code;
    }
    return superGetCode(name);
}

function getAllNames(): string[] {
    const extraResults = Object.keys(EXTRA_LANGUAGE_LIST).map((code) => {
        return EXTRA_LANGUAGE_LIST[code].name;
    });
    const results = superGetAllNames();
    extraResults.forEach((item) => {
        if (results.indexOf(item) < 0) {
            results.push(item);
        }
    });
    return results;
}

function getAllNativeNames(): string[] {
    const extraResults = Object.keys(EXTRA_LANGUAGE_LIST).map((code) => {
        return EXTRA_LANGUAGE_LIST[code].nativeName;
    });
    const results = superGetAllNativeNames();
    extraResults.forEach((item) => {
        if (results.indexOf(item) < 0) {
            results.push(item);
        }
    });
    return results;
}

function getAllCodes(): string[] {
    const extraResults = Object.keys(EXTRA_LANGUAGE_LIST);
    const results = superGetAllCodes();
    extraResults.forEach((item) => {
        if (results.indexOf(item) < 0) {
            results.push(item);
        }
    });
    return results;
}

function validate(code: string) {
    if (Object.prototype.hasOwnProperty.call(EXTRA_LANGUAGE_LIST, code)) {
        return true;
    }
    return superValidate(code);
}

if (ISO6391.getNativeName !== getNativeName) {
    Object.assign(ISO6391, {
        getNativeName,
        getName,
        getCode,
        getAllNames,
        getAllNativeNames,
        getAllCodes,
        validate
    });
}
