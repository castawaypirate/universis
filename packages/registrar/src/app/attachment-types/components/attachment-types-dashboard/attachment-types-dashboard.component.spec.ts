import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachmentTypesDashboardComponent } from './attachment-types-dashboard.component';

describe('AttachmentTypesDashboardComponent', () => {
  let component: AttachmentTypesDashboardComponent;
  let fixture: ComponentFixture<AttachmentTypesDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachmentTypesDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentTypesDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
