import {NgModule, OnInit} from '@angular/core';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule
  ],
  declarations: [
  ],
  exports: [
  ],
  providers: [
  ]
})

export class AttachmentTypesSharedModule  implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading courses module');
      console.error(err);
    });
  }

  async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`./i18n/attachment-types.${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
  }

}
