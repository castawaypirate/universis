import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  ViewChild
} from '@angular/core';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult,
  ActivatedTableService,
  AdvancedSearchFormComponent,
  AdvancedTableSearchComponent
} from '@universis/ngx-tables';
import { LoadingService } from '@universis/common';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ActiveDepartmentService } from '../../../registrar-shared/services/activeDepartmentService.service';

import * as TABLE_CONFIG from './student-additional-info-table.config.list.json';
import * as SEARCH_CONFIG from './student-additional-info-table.search.list.json';

@Component({
  selector: 'app-student-additional-info-table',
  templateUrl: './student-additional-info-table.component.html'
})
export class StudentAdditionalInfoTableComponent implements OnInit, OnDestroy {
  private dataSubscription: Subscription;
  private fragmentSubscription: Subscription;
  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>TABLE_CONFIG;
  public recordsTotal: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  constructor(
    private _loadingService: LoadingService,
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activeDepartmentService: ActiveDepartmentService,
    private _activatedTable: ActivatedTableService
  ) {}

  async ngOnInit() {
    this._loadingService.showLoading();
    const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    this._activatedTable.activeTable = this.table;
    this.table.query = this._context.model('studentInformations')
      .asQueryable()
      .where('student/department')
      .equal(activeDepartment.id)
      .prepare();
    this.table.config = AdvancedTableConfiguration.cast(TABLE_CONFIG);
    this.table.ngOnInit();

    this.searchConfiguration = SEARCH_CONFIG;
    this.search.form = this.searchConfiguration;
    this.search.ngOnInit();

    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.table.fetch(true);
      }
    });

    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      if (data.tableConfiguration) {
        this.table.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
        this.advancedSearch.text = null;
        this.table.reset(true);
      }
    })

    this._loadingService.hideLoading();
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }
}
