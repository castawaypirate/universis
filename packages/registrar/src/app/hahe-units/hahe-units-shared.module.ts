import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';
import { SettingsService } from '../settings-shared/services/settings.service';
import { SettingsSharedModule } from '../settings-shared/settings-shared.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SettingsSharedModule
  ],
  declarations: []
})
export class HaheUnitsSharedModule {
    static forRoot(): ModuleWithProviders<HaheUnitsSharedModule> {
      return {
        ngModule: HaheUnitsSharedModule
      };
    }
    constructor(private translateService: TranslateService, private settings: SettingsService) {
          const sources = environment.languages.map((language: string) => {
            return import(`./i18n/hahe-units.${language}.json`).then((translations) => {
              this.translateService.setTranslation(language, translations, true);
              return Promise.resolve();
            }).catch((err: Error) => {
              console.error('HaheUnitsSharedModule', `An error occurred while loading translations for language ${language}`);
              console.error(err);
              return Promise.resolve();
            });
          });
          Promise.all(sources).then(() => {
            this.translateService.onLangChange.subscribe(() => {
              const Settings: any = this.translateService.instant('Settings');
              this.settings.addSection({
                name: 'HaheUnits',
                description: Settings.Lists.HaheUnits.Description,
                longDescription: Settings.Lists.HaheUnits.LongDescription,
                category: 'Departments',
                url: '/departments/configuration/hahe-units/list',
                dependencies: [
                  'HaheService'
                ]
              });
            });          });
    }
}
