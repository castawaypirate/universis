import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-users-dashboard-overview',
  templateUrl: './users-dashboard-overview.component.html',
  styleUrls: ['./users-dashboard-overview.component.scss']
})
export class UsersDashboardOverviewComponent implements OnInit {
  public user: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    if (this._activatedRoute.snapshot.params.id) {
      this.user = await this._context.model('users')
        .where('id').equal(this._activatedRoute.snapshot.params.id)
        .expand('groups, departments')
        .getItem();
    }
  }
}
