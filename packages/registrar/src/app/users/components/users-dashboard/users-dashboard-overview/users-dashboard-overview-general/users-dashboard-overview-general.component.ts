import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-users-dashboard-overview-general',
  templateUrl: './users-dashboard-overview-general.component.html',
  styleUrls: ['./users-dashboard-overview-general.component.scss']
})
export class UsersDashboardOverviewGeneralComponent implements OnInit {

  @Input() user: any;
  constructor() { }

  ngOnInit() {
    console.log(this.user);
  }

}
