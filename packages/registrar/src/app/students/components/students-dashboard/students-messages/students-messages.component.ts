import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {LoadingService} from '@universis/common';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-messages',
  templateUrl: './students-messages.component.html',
  styleUrls: ['./students-messages.component.scss']
})

export class StudentsMessagesComponent implements OnInit, OnDestroy  {

  public studentMessages: any;
  private subscription: Subscription;

  constructor(private _context: AngularDataContext,  private _activatedRoute: ActivatedRoute, private _loadingService: LoadingService) { }

  async ngOnInit() {
    this._loadingService.showLoading();
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentMessages = await this._context.model('Students/' + params.id + '/messages')
        .asQueryable()
        .expand('action, sender', 'attachments')
        .orderByDescending('dateCreated')
        .take(-1)
        .getItems();
      this._loadingService.hideLoading();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
