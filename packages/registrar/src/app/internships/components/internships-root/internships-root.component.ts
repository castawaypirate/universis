import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { cloneDeep, template } from 'lodash';
import * as INTERNSHIPS_LIST_CONFIG from '../internships-table/internships-table.config.list.json';
import { Subscription } from 'rxjs';
import { UserActivityService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-internships-root',
  templateUrl: './internships-root.component.html',
})
export class InternshipsRootComponent implements OnInit, OnDestroy {

  @Input() model: any;
  public config: any;
  public actions: any[];
  public allowedActions: any[];
  public edit: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService) { }

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('Internships')
        .where('id').equal(params.id)
        .expand('status,internshipPeriod,department,info,student($expand=person)')
        .getItem();
      if (this.model) {
        this._userActivityService.setItem({
          category: this._translateService.instant('Internships.Title'),
          description: this._translateService.instant(this.model.student.person.familyName + ' ' + this.model.student.person.givenName),
          url: window.location.hash.substring(1), // get the path after the hash
          dateCreated: new Date
        });

        // @ts-ignore
        this.config = cloneDeep(INTERNSHIPS_LIST_CONFIG as TableConfiguration);

        if (this.config.columns && this.model) {
          // get actions from config file
          this.actions = this.config.columns.filter(x => {
            return x.actions;
          })
            // map actions
            .map(x => x.actions)
            // get list items
            .reduce((a, b) => b, 0);

          // filter actions with student permissions
          this.allowedActions = this.actions.filter(x => {
            if (x.role) {
              if (x.role === 'action') {
                return x;
              }
            }
          });
          this.edit = this.actions.find(x => {
            if (x.role === 'edit') {
              x.href = template(x.href)(this.model);
              return x;
            }
          });
          this.actions = this.allowedActions;
          this.actions.forEach(action => {
            action.href = template(action.href)(this.model);
          });

        }
      }
    });

  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
