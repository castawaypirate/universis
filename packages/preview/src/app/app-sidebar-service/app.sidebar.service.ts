import {Injectable} from '@angular/core';
import {APP_SIDEBAR_ITEMS} from './app.sidebar.service.routes';

export declare interface AppSidebarNavigationItem {
  name: string;
  url: string;
  index?: number;
  children?: Array<AppSidebarNavigationItem>;
}

@Injectable({
  providedIn: 'root'
})
export class AppSidebarService {

  public navigationItems: Array<AppSidebarNavigationItem> = [];
  constructor() {
    const items: any = APP_SIDEBAR_ITEMS;
    this.addRange(items);
  }

  /**
   * Adds one or more navigation items to application sidebar
   * @param item
   */
  public add(...item: AppSidebarNavigationItem[]): void {
    // get translations keys
    const keys = item.map( x => {
      return x.name;
    });
    // add navigation items to array
    this.navigationItems.push.apply(this.navigationItems, item.map(navigationItem => {
      // translate name
      const x = Object.assign(navigationItem, {
        name: navigationItem.name
      });
      if (Array.isArray(x.children)) {
        // translate children
        x.children = x.children.map( child => {
          return Object.assign(child, {
            name: child.name
          });
        });
      }
      return x;
    }));
  }

  /**
   * Adds a collection of navigation items to application sidebar
   * @param items
   */
  public addRange(items: Array<AppSidebarNavigationItem>): void {
    return this.add.apply(this, items);
  }

}
